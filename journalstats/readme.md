# Journal statistics

The purpose of the code:
1. Join journal-level metrics from various sources
2. Allow journal grouping according to various categories
3. Calculate derived journal metrics according to the groupings

Output of each of these code blocks should be exported in a commonly 
readable format (*.csv) and used as an input for the next step.

## Joining journal-level metrics from various sources

Journals are to be joined according to their ISSN.

If more ISSNs exist for a journal, the one used by Web of Science is 
the primary one.

The joined record has to conserve the year for which the metrics apply. Therefore, ISSN is not a unique key for a record, but combination of the primary ISSN and the year is.

TODO: A helper function is to be developed that translates the non-primary ISSNs to the primary one. 


